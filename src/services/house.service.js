import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://find-home-apps-staging.herokuapp.com/api/v1/houses/';

class HouseService {
    getAllHouses() {
        return axios.get(API_URL, {
            headers: authHeader()
        }).then(res => {
            console.log(res.data)
            return res.data
        }).catch(error => {
            console.log("Service Error: ", error, '\nHeader: ', authHeader())
        })
    }
}

export default new HouseService();
