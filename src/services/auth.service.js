import axios from 'axios'

const API_URL = 'https://find-home-apps-staging.herokuapp.com/api/v1/users/'

class AuthService {
    login(user) {
        return axios.post(API_URL + 'login', {
            email: user.email,
            password: user.password
        }).then(res => {
            if (res.data) {
                console.log(res.data)
                localStorage.setItem('user', JSON.stringify(res.data));
            }
            return res.data
        })
    }

    logout() {
        localStorage.removeItem('user');
        localStorage.clear()
    }

    register(user) {
        return axios.post(API_URL + 'signup', {
            name: user.name,
            phone: user.phone,
            email: user.email,
            role: user.role,
            gender: user.gender,
            birthdate: user.birthdate
        }).then(res => {
            console.log(res)
            return res
        }).catch(error => {
            console.log(error)
        })
    }
}

export default new AuthService();