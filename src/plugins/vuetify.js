import Vue from 'vue';
// import Vuetify from 'vuetify/lib/framework'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify, {
    iconfont: 'md',
});

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#34495E',
                secondary: '#b0bec5',
                anchor: '#8c9eff',
            },
        },
    },
});
