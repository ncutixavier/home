//authentication module
import AuthService from '../services/auth.service'

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
    ? { status: { loggedIn: true }, user }
    : { status: { loggedIn: false }, user: null }

export const auth = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
                return AuthService.login(user).then(
                    user => {
                        commit('loginSuccess', user)
                        resolve(user)
                    },
                    error => {
                        commit('loginFailure', error)
                        reject(error)
                    }
                )
            })
        },
        logout({ commit }) {
            AuthService.logout()
            commit('logout')
        },
        register({ commit }, user) {
            return new Promise((resolve, reject) => {
                return AuthService.register(user).then(
                    response => {
                        commit('registerSuccess')
                        resolve(response)
                    },
                    error => {
                        commit('registerFailure')
                        reject(error)
                    }
                )
            })

        }
    },
    mutations: {
        loginSuccess(state, user) {
            state.status.loggedIn = true
            state.user = user
        },
        loginFailure(state) {
            state.status.loggedIn = false
            state.user = null
        },
        logout(state) {
            state.status.loggedIn = false
            state.user = null
        },
        registerSuccess(state) {
            state.status.loggedIn = false
        },
        registerFailure(state) {
            state.status.loggedIn = false
        },
    },
}
